package com.lovol.mySmartCameraCordova;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Intent;


import com.lovol.mySmartCamera.CameraActivity;
import com.lovol.tool.CordovaUtil;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartCameraAndroid extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if(action.equals("startScActvity")){
            this.startScActvity(callbackContext);
            return true;
          }
        return false;
    }

    public void startScActvity(CallbackContext callbackContext){

        //把CallbackContext存到单例工具类
        CordovaUtil.getInstance().setCallbackContext(callbackContext);
    
    
        Activity activity = this.cordova.getActivity();
    
        Intent intent=new Intent(activity,CameraActivity.class);
        // 启动指定Activity并等待返回的结果，其中0是请求码，用于标识该请求
        intent.setPackage(this.cordova.getActivity().getApplicationContext().getPackageName());
    
        //启动Activity
        this.cordova.startActivityForResult((CordovaPlugin) this, intent, 0);
    
        //保持cordova的Activity生命周期，用于随时返回结果到js端代码回调
        this.cordova.setActivityResultCallback(this);
    
      }

}
