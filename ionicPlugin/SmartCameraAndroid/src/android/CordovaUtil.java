package com.lovol.tool;

import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONObject;

public class CordovaUtil {
  private CallbackContext callbackContext;

  private static class CordovaUtilHolder {
    public final static CordovaUtil INSTANCE = new CordovaUtil();
  }

  private CordovaUtil() { }

  public static final CordovaUtil getInstance() {
    return CordovaUtilHolder.INSTANCE;
  }

  public CallbackContext getCallbackContext() {
    return callbackContext;
  }

  public void setCallbackContext(CallbackContext callbackContext) {
    this.callbackContext = callbackContext;
  }

  public static void CallbackSuccess(){ getInstance().getCallbackContext().success(); }

  public static void CallbackSuccess(int message){ getInstance().getCallbackContext().success(message); }

  public static void CallbackSuccess(String message){ getInstance().getCallbackContext().success(message); }

  public static void CallbackSuccess(JSONObject message){ getInstance().getCallbackContext().success(message); }

  public static void CallbackSuccess(JSONArray message){ getInstance().getCallbackContext().success(message); }

  public static void CallbackError(JSONObject message){ getInstance().getCallbackContext().error(message); }

  public static void CallbackError(String message){ getInstance().getCallbackContext().error(message); }

  public static void CallbackError(int message){ getInstance().getCallbackContext().error(message); }
}
