package com.lovol.mySmartCamera;


import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CordovaUtil {


    private static Method _CallbackSuccess;
    private static Method _CallbackError;

    static {

        try {
            Class<?> CordovaUtilClass = Class.forName("com.lovol.tool.CordovaUtil");
            _CallbackSuccess = CordovaUtilClass.getMethod("CallbackSuccess", String.class);
            _CallbackError = CordovaUtilClass.getMethod("CallbackError", String.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void CallbackSuccess(String message){
        try {
            _CallbackSuccess.invoke(null,message);
        } catch (IllegalAccessException e) {
            Log.i("CordovaUtil","CallbackSuccess[无法访问com.lovol.tool.CordovaUtil类中的CallbackSuccess方法]");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Log.i("CordovaUtil","CallbackSuccess[com.lovol.tool.CordovaUtil类的CallbackSuccess方法中异常捕获]");
            e.printStackTrace();
        }
    }

    public static void CallbackError(String message){
        try {
            _CallbackError.invoke(null,message);
        } catch (IllegalAccessException e) {
            Log.i("CordovaUtil","CallbackError[无法访问com.lovol.tool.CordovaUtil类中的CallbackError方法]");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Log.i("CordovaUtil","CallbackError[com.lovol.tool.CordovaUtil类的CallbackError方法中异常捕获]");
            e.printStackTrace();
        }
    }



}