## 基于SmartCamera打包的Cordova插件

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

>基于SmartCamera Android 相机拓展库 包装成Cordova插件

[SmartCamera Android 库地址](https://github.com/pqpo/SmartCamera)

供Cordova android程序直接调用


>ionicPlugin 文件夹下是Cordova插件


>aarProject 文件夹下SmartCamera项目是Cordova插件调用的aar库代码,编译出的aar包替换掉Cordova插件中的mysmartcameralib-release.aar即可



